import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { FormsModule }   from '@angular/forms';

import { AppComponent }   from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';

import { DashboardModule } from './dashboard/dashboard.module';
import { SidebarModule } from './sidebar/sidebar.module';
import { FooterModule } from './shared/footer/footer.module';
import { NavbarModule} from './shared/navbar/navbar.module';
import { AuthModule } from './auth/auth.module';
import {APP_ROUTES} from "./app.routes";
import {LoginComponent} from "./auth/login.component";

@NgModule({
    imports:      [
        AuthModule,
        BrowserModule,
        DashboardModule,
        SidebarModule,
        NavbarModule,
        FooterModule,
        FormsModule,
        RouterModule.forRoot(APP_ROUTES)
    ],
    declarations: [ AppComponent, DashboardComponent, LoginComponent ],
    bootstrap:    [ AppComponent ]
})
export class AppModule { }
