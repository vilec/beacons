import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Router } from '@angular/router';

@Injectable()
export class UserService {
  private loggedIn = false;

  constructor(private http: Http, private router : Router) {
    this.loggedIn = !!localStorage.getItem('id_token');
  }

  login(username : String, password : String) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    this.http.put('/api/login', JSON.stringify({ username, password }), { headers })
      .toPromise()
      .then(response => {
        if (response.json().token) {
          localStorage.setItem('id_token', response.json().token);
          this.loggedIn = true;
          this.router.navigate(['/']);
        }
      });
  }

  logout() {
   localStorage.removeItem('id_token');
   this.loggedIn = false;
   this.router.navigate(['/login']);
  }

  isLoggedIn() {
    this.loggedIn = !!localStorage.getItem('id_token');
    return this.loggedIn;
  }
}
