import { MenuType, RouteInfo } from './sidebar.metadata';

export const ROUTES: RouteInfo[] = [
    { path: '/dashboard', title: 'Dashboard', menuType: MenuType.LEFT, icon: 'pe-7s-graph' },
    { path: '/beacons', title: 'Beacons', menuType: MenuType.LEFT, icon: 'pe-7s-signal' },
    { path: '/contents', title: 'Content', menuType: MenuType.LEFT, icon: 'pe-7s-news-paper' },
   /* { path: '/user', title: 'User profile', menuType: MenuType.LEFT, icon:'pe-7s-user' },
    { path: '/table', title: 'Table List', menuType: MenuType.LEFT, icon:'pe-7s-note2' },
    { path: '/typography', title: 'Typography', menuType: MenuType.LEFT, icon:'pe-7s-news-paper' },
    { path: '/icons', title: 'Icons', menuType: MenuType.LEFT, icon:'pe-7s-science' },
    { path: '/maps', title: 'Maps', menuType: MenuType.LEFT, icon:'pe-7s-map-marker' },
    { path: '/login', title: 'Go to Login', menuType: MenuType.LEFT, icon:'pe-7s-bell' }*/
];
