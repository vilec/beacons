import {Component, OnInit} from "@angular/core"
import {Router, ActivatedRoute} from "@angular/router"
import {LocationStrategy, PlatformLocation, Location} from "@angular/common"
declare var $:JQueryStatic;

@Component({
  selector: 'my-app',
  moduleId: module.id,
  templateUrl: 'app.component.html'
})

export class AppComponent implements OnInit {
  ngOnInit() {
     $.getScript('../assets/js/light-bootstrap-dashboard.js');
  }

  public isRoute(path: string) {
    if (path === window.location.pathname) {
      return true
    }
    else {
      return false
    }
  }
}
