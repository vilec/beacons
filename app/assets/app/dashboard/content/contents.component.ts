import {Component, OnInit,AfterViewInit,trigger,state,style,transition,animate,keyframes} from '@angular/core';
import { Router }            from '@angular/router';

import { ContentService }         from './contents.service';
import {Content} from "./content";


@Component({
    moduleId: module.id,
    selector: 'contents-cmp',
    templateUrl: 'contents.component.html',
    providers: [ContentService],
    animations: [
        trigger('table', [
            state('*', style({
                '-ms-transform': 'translate3D(0px, 0px, 0px)',
                '-webkit-transform': 'translate3D(0px, 0px, 0px)',
                '-moz-transform': 'translate3D(0px, 0px, 0px)',
                '-o-transform':'translate3D(0px, 0px, 0px)',
                transform:'translate3D(0px, 0px, 0px)',
                opacity: 1})),
                transition('void => *', [
                    style({opacity: 0,
                        '-ms-transform': 'translate3D(0px, 150px, 0px)',
                        '-webkit-transform': 'translate3D(0px, 150px, 0px)',
                        '-moz-transform': 'translate3D(0px, 150px, 0px)',
                        '-o-transform':'translate3D(0px, 150px, 0px)',
                        transform:'translate3D(0px, 150px, 0px)',
                    }),
                    animate('0.3s 0s ease-out')
                ])
        ])
    ]
})

export class ContentsComponent implements OnInit {
  contents: Content[];

  constructor (private contentService: ContentService,  private router: Router) { }

  ngOnInit(): void {
    this.contentService
      .getContents()
      .then(b => this.contents = b);
  }

  addContent(): void {
    this.router.navigate(['/content']);
  }

  showContent(content: Content): void {
    this.router.navigate(['/content', content.id]);
  }

}
