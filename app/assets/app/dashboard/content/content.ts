export class Content {
  id: number;
  name: string;
  content: string;
  type: string;
  url: string;
  title: string
}

