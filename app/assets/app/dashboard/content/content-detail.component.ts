import {animate, Component, OnInit, state, style, transition, trigger} from "@angular/core";
import {ActivatedRoute, Router} from "@angular/router";
import { DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';
import {Content} from "./content";
import {ContentService} from "./contents.service";

import "rxjs/add/operator/switchMap";

declare var $:JQueryStatic;

@Component({
  moduleId: module.id,
  selector: 'content-cmp',
  templateUrl: 'content-detail.component.html',
  providers: [ContentService],
  animations: [
    trigger('table', [
      state('*', style({
        '-ms-transform': 'translate3D(0px, 0px, 0px)',
        '-webkit-transform': 'translate3D(0px, 0px, 0px)',
        '-moz-transform': 'translate3D(0px, 0px, 0px)',
        '-o-transform': 'translate3D(0px, 0px, 0px)',
        transform: 'translate3D(0px, 0px, 0px)',
        opacity: 1
      })),
      transition('void => *', [
        style({
          opacity: 0,
          '-ms-transform': 'translate3D(0px, 150px, 0px)',
          '-webkit-transform': 'translate3D(0px, 150px, 0px)',
          '-moz-transform': 'translate3D(0px, 150px, 0px)',
          '-o-transform': 'translate3D(0px, 150px, 0px)',
          transform: 'translate3D(0px, 150px, 0px)',
        }),
        animate('0.3s 0s ease-out')
      ])
    ])
  ]
})

export class ContentDetailComponent implements OnInit {
  content: Content;
  addMode: boolean;
  previewURL: SafeResourceUrl;

  constructor (private contentService: ContentService,  private route: ActivatedRoute,
               private sanitizer: DomSanitizer, private router: Router) { }

  ngOnInit(): void {
    var contentId: number;
    this.route.params.subscribe(params => {
      contentId = +params["id"];
    });

    if (contentId) {
      this.contentService.getContent(contentId)
        .then(c => {
          this.content = c;
          this.refreshUrl();
        });
    } else {
      this.content = new Content();
      this.addMode = true;
    }
  }

  refreshUrl(): void {
    this.previewURL = this.sanitizer.bypassSecurityTrustResourceUrl("/" + this.content.id);
  }

  save(): void {
    if (this.addMode) {
      this.contentService.create(this.content)
        .then(() => this.goBack());
    } else {
      this.contentService.update(this.content)
        .then(() => {
          this.refreshUrl();
          $.notify({
            icon: "pe-7s-cup",
            message: "Content saved"
          },{
            type: 'success'
          });
        });
    }

  }

  goBack(): void {
    this.router.navigate(['/contents']);
  }

}


/*
 Copyright 2017 Google Inc. All Rights Reserved.
 Use of this source code is governed by an MIT-style license that
 can be found in the LICENSE file at http://angular.io/license
 */
