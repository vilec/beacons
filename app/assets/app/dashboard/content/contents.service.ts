import { Injectable }    from '@angular/core';
import { Headers } from '@angular/http';
import { AuthHttp } from 'angular2-jwt';

import 'rxjs/add/operator/toPromise';
import {Content} from "./content";


@Injectable()
export class ContentService {

  private headers = new Headers({'Content-Type': 'application/json'});
  private contentsUrl = 'api/contents';  // URL to web api
  private contentUrl = 'api/content';  // URL to web api

  constructor(private authHttp: AuthHttp) { }

  getContents(): Promise<Content[]> {
    return this.authHttp.get(this.contentsUrl)
      .toPromise()
      .then(response => response.json() as Content[])
      .catch(this.handleError);
  }

  update(content: Content): Promise<Content> {
    const url = `${this.contentUrl}`;
    return this.authHttp
      .put(url, JSON.stringify(content), {headers: this.headers})
      .toPromise()
      .then(() => content)
      .catch(this.handleError);
  }

  create(content: Content): Promise<Content> {
    const url = `${this.contentUrl}`;
    return this.authHttp
      .post(url, JSON.stringify(content), {headers: this.headers})
      .toPromise()
      .then(() => content)
      .catch(this.handleError);
  }

  getContent(id: number): Promise<Content> {
    const url = `${this.contentUrl}/${id}`;
    return this.authHttp.get(url)
      .toPromise()
      .then(response => response.json() as Content)
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}

