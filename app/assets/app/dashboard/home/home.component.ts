import {Component, OnInit,trigger,state,style,transition,animate,keyframes, group,ViewChild, ViewChildren, ElementRef, SimpleChanges,SimpleChange} from '@angular/core';
//import initDemo = require('assets/js/charts.js');
//import initNotify = require('assets/public/js/notify.js');
declare var $:JQueryStatic;
import { BaseChartDirective } from '../../charts';


import {ReportService} from "./report.service";
import {Stats} from "./stats";
import {TopReport} from "./top-report";


@Component({
    moduleId: module.id,
    selector: 'home-cmp',
    templateUrl: 'home.component.html',
    providers: [ReportService],
    animations: [
        trigger('cardemail', [
            state('*', style({
                '-ms-transform': 'translate3D(0px, 0px, 0px)',
                '-webkit-transform': 'translate3D(0px, 0px, 0px)',
                '-moz-transform': 'translate3D(0px, 0px, 0px)',
                '-o-transform':'translate3D(0px, 0px, 0px)',
                transform:'translate3D(0px, 0px, 0px)',
                opacity: 1})),
                transition('void => *', [
                    style({opacity: 0,
                        '-ms-transform': 'translate3D(0px, 150px, 0px)',
                        '-webkit-transform': 'translate3D(0px, 150px, 0px)',
                        '-moz-transform': 'translate3D(0px, 150px, 0px)',
                        '-o-transform':'translate3D(0px, 150px, 0px)',
                        transform:'translate3D(0px, 150px, 0px)',
                    }),
                    animate('0.3s 0s ease-out')
                ])
        ]),
        trigger('carduser', [
            state('*', style({
                '-ms-transform': 'translate3D(0px, 0px, 0px)',
                '-webkit-transform': 'translate3D(0px, 0px, 0px)',
                '-moz-transform': 'translate3D(0px, 0px, 0px)',
                '-o-transform':'translate3D(0px, 0px, 0px)',
                transform:'translate3D(0px, 0px, 0px)',
                opacity: 1})),
                transition('void => *', [
                    style({opacity: 0,
                        '-ms-transform': 'translate3D(0px, 150px, 0px)',
                        '-webkit-transform': 'translate3D(0px, 150px, 0px)',
                        '-moz-transform': 'translate3D(0px, 150px, 0px)',
                        '-o-transform':'translate3D(0px, 150px, 0px)',
                        transform:'translate3D(0px, 150px, 0px)',
                    }),
                    animate('0.3s 0.25s ease-out')
                ])
        ]),
        trigger('card2014sales', [
            state('*', style({
                '-ms-transform': 'translate3D(0px, 0px, 0px)',
                '-webkit-transform': 'translate3D(0px, 0px, 0px)',
                '-moz-transform': 'translate3D(0px, 0px, 0px)',
                '-o-transform':'translate3D(0px, 0px, 0px)',
                transform:'translate3D(0px, 0px, 0px)',
                opacity: 1})),
                transition('void => *', [
                    style({opacity: 0,
                        '-ms-transform': 'translate3D(0px, 150px, 0px)',
                        '-webkit-transform': 'translate3D(0px, 150px, 0px)',
                        '-moz-transform': 'translate3D(0px, 150px, 0px)',
                        '-o-transform':'translate3D(0px, 150px, 0px)',
                        transform:'translate3D(0px, 150px, 0px)',
                    }),
                    animate('0.3s 0.5s ease-out')
                ])
        ]),
        trigger('cardtasks', [
            state('*', style({
                '-ms-transform': 'translate3D(0px, 0px, 0px)',
                '-webkit-transform': 'translate3D(0px, 0px, 0px)',
                '-moz-transform': 'translate3D(0px, 0px, 0px)',
                '-o-transform':'translate3D(0px, 0px, 0px)',
                transform:'translate3D(0px, 0px, 0px)',
                opacity: 1})),
                transition('void => *', [
                    style({opacity: 0,
                        '-ms-transform': 'translate3D(0px, 150px, 0px)',
                        '-webkit-transform': 'translate3D(0px, 150px, 0px)',
                        '-moz-transform': 'translate3D(0px, 150px, 0px)',
                        '-o-transform':'translate3D(0px, 150px, 0px)',
                        transform:'translate3D(0px, 150px, 0px)',
                    }),
                    animate('0.3s 0.75s ease-out')
                ])
        ])
    ]


})

export class HomeComponent implements OnInit{

  constructor (private reportService: ReportService) { }

  topContent: TopReport[];
  topBeacon: TopReport[];
  stats: Stats;

  lineChartData: Array<any> = [{data : []}];
  public lineChartData2:Array<any> = [
    {data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A'},
    {data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B'},
    {data: [18, 48, 77, 9, 100, 27, 40], label: 'Series C'}
  ];
  public lineChartLabels:Array<any> = ['January', 'February', 'March'];
  public lineChartOptions:any = {
    responsive: true,
    legend: {
      position: 'bottom'
    },
    animation: {
      animateRotate: true
    }
};
  public lineChartColors:Array<any> = [{backgroundColor: ['#8872f9','#39edea', '#f86486']}];
    public lineChartColors2:Array<any> = [
    { // red
      backgroundColor: '#FB404B',
      borderColor: '#FB404B',
      pointBackgroundColor: '#FB404B',
      pointBorderColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    { // blue
      backgroundColor: '#23CCEF',
      borderColor: '#23CCEF',
      pointBackgroundColor: '#23CCEF',
      pointBorderColor: '#fff',
      pointHoverBorderColor: 'rgba(77,83,96,1)'
    },
    { // grey
      backgroundColor: '#FFA534',
      borderColor: '#FFA534',
      pointBackgroundColor: '#FFA534',
      pointBorderColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    }
  ];
  public lineChartLegend:boolean = true;
  public lineChartType:string = 'pie';

  public lineChartData3:Array<any> = [{data : []}];
  public lineChartLabels3:Array<any> = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];
  public lineChartOptions3:any = {
    responsive: true,
    legend: {
      position: 'bottom'
    }
  };

  @ViewChild("pieChart", {read : BaseChartDirective}) pieChart: BaseChartDirective;


  @ViewChild("lineChart") lineChart: ElementRef;


  @ViewChild("lineChart", {read : BaseChartDirective}) chart: BaseChartDirective;

  ngAfterViewInit() { // wait for the view to init before using the element

    let ctx: CanvasRenderingContext2D = this.lineChart.nativeElement.getContext("2d");
    let gradient: CanvasGradient = ctx.createLinearGradient(0, 0, 0, 300);
    gradient.addColorStop(0, '#6b51f2');
    gradient.addColorStop(1, '#c7bcff');
    this.lineChartColors3[0].backgroundColor = gradient;

    let gradient2: CanvasGradient = ctx.createLinearGradient(0, 0, 0, 300);
    gradient2.addColorStop(0, '#54fcff');
    gradient2.addColorStop(1, '#15d2d2');
    this.lineChartColors3[1].backgroundColor = gradient2;

    let gradient3: CanvasGradient = ctx.createLinearGradient(0, 0, 0, 300);
    gradient3.addColorStop(0, '#fc6586');
    gradient3.addColorStop(1, '#c02150');
    this.lineChartColors3[2].backgroundColor = gradient3;

    let gradient4: CanvasGradient = ctx.createLinearGradient(0, 0, 0, 300);
    gradient4.addColorStop(0, '#ffd96f');
    gradient4.addColorStop(1, '#ffbc00');
    this.lineChartColors3[3].backgroundColor = gradient4;

    this.chart.ngOnChanges({} as SimpleChanges);
  }

  public lineChartColors3:Array<any> = [
    { // grey
      backgroundColor: '#c2b0ea',
      borderColor: '#c2b0ea',
      pointBackgroundColor: '#ffffff',
      pointBorderColor: '#c2b0ea',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    { // dark grey
      backgroundColor: '#acd8d8',
      borderColor: '#acd8d8',
      pointBackgroundColor: '#ffffff',
      pointBorderColor: '#acd8d8',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    { // grey
      backgroundColor: '#e3aab6',
      borderColor: '#e3aab6',
      pointBackgroundColor: '#ffffff',
      pointBorderColor: '#e3aab6',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    }
    ,
    { // grey
      backgroundColor: '#efe0b5',
      borderColor: '#efe0b5',
      pointBackgroundColor: '#ffffff',
      pointBorderColor: '#efe0b5',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    }
  ];
  public lineChartLegend3:boolean = true;
  public lineChartType3:string = 'line';

  ngOnInit() {
      this.reportService
        .getOsBreakdown()
        .then(r => {this.lineChartData = [{data : r.data}];  this.lineChartLabels = r.labels; });

      this.reportService
      .getLastDaysReport()
      .then(r => {this.lineChartData3 = r.series; this.lineChartLabels3 = r.labels; });

      this.reportService.getStats().then(r => {this.stats = r});
      this.reportService.getTopContent().then(r => {this.topContent = r});
      this.reportService.getTopBeacon().then(r => {this.topBeacon = r});

        $.getScript('../../../assets/js/bootstrap-checkbox-radio-switch.js');
        $.getScript('../../../assets/js/light-bootstrap-dashboard.js');

      /*  $('[data-toggle="checkbox"]').each(function () {
            if($(this).data('toggle') == 'switch') return;

            var $checkbox = $(this);
            $checkbox.checkbox();
        });*/
       // var Chart = require('/assets/js/charts.js')
        var Notify = require('/assets/js/notify.js')
       // Chart();
        Notify();


    }
}
