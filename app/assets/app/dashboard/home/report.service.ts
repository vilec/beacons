import { Injectable }    from '@angular/core';
import { AuthHttp } from 'angular2-jwt';

import 'rxjs/add/operator/toPromise';
import {PieReport} from "./pie-report";
import {ChartReport} from "./chart-report";
import {Stats} from "./stats";
import {TopReport} from "./top-report";


@Injectable()
export class ReportService {

  private url = 'api/report';  // URL to web api

  constructor(private authHttp: AuthHttp) { }

  getTopContent(): Promise<TopReport[]>{
    return this.authHttp.get(this.url + "/topContent")
      .toPromise()
      .then(response => response.json() as TopReport[])
      .catch(this.handleError);
  }

   getTopBeacon(): Promise<TopReport[]>{
    return this.authHttp.get(this.url + "/topBeacon")
      .toPromise()
      .then(response => response.json() as TopReport[])
      .catch(this.handleError);
  }

  getStats(): Promise<Stats>{
    return this.authHttp.get(this.url + "/stats")
      .toPromise()
      .then(response => response.json() as Stats)
      .catch(this.handleError);
  }

  getOsBreakdown(): Promise<PieReport> {
    return this.authHttp.get(this.url)
      .toPromise()
      .then(response => response.json() as PieReport)
      .catch(this.handleError);
  }

  getLastDaysReport(): Promise<ChartReport> {
    return this.authHttp.get(this.url + "/lastDays")
      .toPromise()
      .then(response => response.json() as ChartReport)
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}

