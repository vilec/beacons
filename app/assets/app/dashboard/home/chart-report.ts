export class ChartReport {
  series: ChartReportSeries[];
  labels: string[];
}

export class ChartReportSeries {
  data: number[];
  label: string;
}

