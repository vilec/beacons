import {Component, OnInit,AfterViewInit,trigger,state,style,transition,animate,keyframes} from '@angular/core';
import { Router }            from '@angular/router';

import { Beacon }                from './beacon';
import { BeaconService }         from './beacons.service';


@Component({
    moduleId: module.id,
    selector: 'beacons-cmp',
    templateUrl: 'beacons.component.html',
    providers: [BeaconService],
    animations: [
        trigger('table', [
            state('*', style({
                '-ms-transform': 'translate3D(0px, 0px, 0px)',
                '-webkit-transform': 'translate3D(0px, 0px, 0px)',
                '-moz-transform': 'translate3D(0px, 0px, 0px)',
                '-o-transform':'translate3D(0px, 0px, 0px)',
                transform:'translate3D(0px, 0px, 0px)',
                opacity: 1})),
                transition('void => *', [
                    style({opacity: 0,
                        '-ms-transform': 'translate3D(0px, 150px, 0px)',
                        '-webkit-transform': 'translate3D(0px, 150px, 0px)',
                        '-moz-transform': 'translate3D(0px, 150px, 0px)',
                        '-o-transform':'translate3D(0px, 150px, 0px)',
                        transform:'translate3D(0px, 150px, 0px)',
                    }),
                    animate('0.3s 0s ease-out')
                ])
        ])
    ]
})

export class BeaconsComponent implements OnInit {
  beacons: Beacon[];

  constructor (private beaconService: BeaconService,  private router: Router) { }

  getBeacons(): void {
    this.beaconService
      .getBeacons()
      .then(b => this.beacons = b);
  }

  addBeacon(): void {
    this.router.navigate(['/beacon']);
  }

  showBeacon(beacon: Beacon): void {
    this.router.navigate(['/beacon', beacon.id]);
  }

  ngOnInit(): void {
    this.getBeacons();
  }

}


/*
 Copyright 2017 Google Inc. All Rights Reserved.
 Use of this source code is governed by an MIT-style license that
 can be found in the LICENSE file at http://angular.io/license
 */
