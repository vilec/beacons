import { Injectable }    from '@angular/core';
import { Headers } from '@angular/http';
import { AuthHttp } from 'angular2-jwt';

import 'rxjs/add/operator/toPromise';

import { Beacon } from './beacon';

@Injectable()
export class BeaconService {

  private headers = new Headers({'Content-Type': 'application/json'});
  private beaconsUrl = 'api/beacons';  // URL to web api
  private beaconUrl = 'api/beacon';  // URL to web api

  constructor(private authHttp: AuthHttp) { }

  getBeacons(): Promise<Beacon[]> {
    return this.authHttp.get(this.beaconsUrl)
      .toPromise()
      .then(response => response.json() as Beacon[])
      .catch(this.handleError);
  }

  update(beacon: Beacon): Promise<Beacon> {
    const url = `${this.beaconUrl}`;
    return this.authHttp
      .put(url, JSON.stringify(beacon), {headers: this.headers})
      .toPromise()
      .then(() => beacon)
      .catch(this.handleError);
  }

  create(beacon: Beacon): Promise<Beacon> {
    const url = `${this.beaconUrl}`;
    return this.authHttp
      .post(url, JSON.stringify(beacon), {headers: this.headers})
      .toPromise()
      .then(() => beacon)
      .catch(this.handleError);
  }

  getBeacon(id: number): Promise<Beacon> {
    const url = `${this.beaconUrl}/${id}`;
    return this.authHttp.get(url)
      .toPromise()
      .then(response => response.json() as Beacon)
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}

