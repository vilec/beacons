import {animate, Component, OnInit, state, style, transition, trigger} from "@angular/core";
import {ActivatedRoute, Router} from "@angular/router";
import {Location} from "@angular/common";

import {Beacon} from "./beacon";
import {BeaconService} from "./beacons.service";

import "rxjs/add/operator/switchMap";
import {Content} from "../content/content";
import {ContentService} from "../content/contents.service";

import { DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';

declare var $:JQueryStatic;

@Component({
  moduleId: module.id,
  selector: 'beacons-cmp',
  templateUrl: 'beacon-detail.component.html',
  providers: [BeaconService, ContentService],
  animations: [
    trigger('table', [
      state('*', style({
        '-ms-transform': 'translate3D(0px, 0px, 0px)',
        '-webkit-transform': 'translate3D(0px, 0px, 0px)',
        '-moz-transform': 'translate3D(0px, 0px, 0px)',
        '-o-transform': 'translate3D(0px, 0px, 0px)',
        transform: 'translate3D(0px, 0px, 0px)',
        opacity: 1
      })),
      transition('void => *', [
        style({
          opacity: 0,
          '-ms-transform': 'translate3D(0px, 150px, 0px)',
          '-webkit-transform': 'translate3D(0px, 150px, 0px)',
          '-moz-transform': 'translate3D(0px, 150px, 0px)',
          '-o-transform': 'translate3D(0px, 150px, 0px)',
          transform: 'translate3D(0px, 150px, 0px)',
        }),
        animate('0.3s 0s ease-out')
      ])
    ])
  ]
})

export class BeaconDetailComponent implements OnInit {
  beacon: Beacon;
  addMode: boolean;
  contents: Content[];
  previewURL: SafeResourceUrl;

  constructor (private beaconService: BeaconService,  private route: ActivatedRoute,
               private location: Location, private contentService: ContentService,
               private sanitizer: DomSanitizer, private router: Router) { }

  ngOnInit(): void {
    this.contentService.getContents()
      .then(c => this.contents = c);
    var beaconId: number;
    this.route.params.subscribe(params => {
      beaconId = +params["id"];
    });

    if (beaconId) {
      this.beaconService.getBeacon(beaconId)
        .then(b => {
          this.beacon = b;
          this.beacon.content = this.beacon.content || { id : -1}
          this.refreshUrl();
        });
    } else {
      this.beacon = new Beacon();
      this.beacon.content = {id : -1};
      this.addMode = true;
    }
  }

  refreshUrl(): void {
    this.previewURL = this.sanitizer.bypassSecurityTrustResourceUrl("/" + this.beacon.code);
  }

  save(): void {
    var beacon = Object.assign({}, this.beacon);
    if (this.beacon.content.id == -1){
        beacon.content = null;
    }
    if (this.addMode) {
      this.beaconService.create(beacon)
        .then(() => this.goBack());
    } else {
      this.beaconService.update(beacon)
        .then(() => {
          this.refreshUrl();
          $.notify({
            icon: "pe-7s-cup",
            message: "Beacon configuration updated"
          },{
            type: 'success'
          });
        });
    }

  }

  goBack(): void {
    this.router.navigate(['/beacons']);
  }

}


/*
 Copyright 2017 Google Inc. All Rights Reserved.
 Use of this source code is governed by an MIT-style license that
 can be found in the LICENSE file at http://angular.io/license
 */
