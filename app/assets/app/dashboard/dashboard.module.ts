import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { AuthGuard }   from '../auth/auth.guard';
import { UserService } from "../auth/user.service";
import { HomeComponent } from './home/home.component';
import { UserComponent } from './user/user.component';
import { IconsComponent } from './icons/icons.component';
import { TableComponent } from './table/table.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { TypographyComponent } from './typography/typography.component';
import { MapsComponent } from './maps/maps.component';
import { BeaconsComponent } from './beacons/beacons.component';
import { BeaconDetailComponent } from './beacons/beacon-detail.component';
import {ContentsComponent} from "./content/contents.component";
import {ContentDetailComponent} from "./content/content-detail.component";
import { ChartsModule } from '../charts';

@NgModule({
    imports: [
        BrowserModule,
        HttpModule,
        FormsModule,
        ChartsModule
    ],
    declarations: [
      HomeComponent,
      UserComponent,
      TableComponent,
      IconsComponent,
      NotificationsComponent,
      TypographyComponent,
      MapsComponent,
      BeaconsComponent,
      ContentsComponent,
      ContentDetailComponent,
      BeaconDetailComponent
    ],
    providers: [
      AuthGuard, UserService
    ]
})

export class DashboardModule{}
