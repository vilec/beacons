import { Route } from '@angular/router';
import {DashboardComponent} from "./dashboard/dashboard.component";
import {AuthGuard} from "./auth/auth.guard";
import {LoginComponent} from "./auth/login.component";
import { HomeComponent } from './dashboard/home/home.component';
import { UserComponent } from './dashboard/user/user.component';
import { IconsComponent } from './dashboard/icons/icons.component';
import { TableComponent } from './dashboard/table/table.component';
import { NotificationsComponent } from './dashboard/notifications/notifications.component';
import { TypographyComponent } from './dashboard/typography/typography.component';
import { MapsComponent } from './dashboard/maps/maps.component';
import { BeaconsComponent } from './dashboard/beacons/beacons.component';
import { BeaconDetailComponent } from './dashboard/beacons/beacon-detail.component';
import { ContentsComponent } from "./dashboard/content/contents.component";
import { ContentDetailComponent } from "./dashboard/content/content-detail.component";

export const APP_ROUTES: Route[] =[
    { path: '' , component: DashboardComponent, canActivate: [AuthGuard],
      children: [
        { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
        { path: 'dashboard', component: HomeComponent },
        { path: 'beacons', component: BeaconsComponent },
        { path: 'beacon', component: BeaconDetailComponent },
        { path: 'beacon/:id', component: BeaconDetailComponent },
        { path: 'contents', component: ContentsComponent },
        { path: 'content', component: ContentDetailComponent },
        { path: 'content/:id', component: ContentDetailComponent },
        { path: 'user', component: UserComponent },
        { path: 'table', component: TableComponent },
        { path: 'icons', component: IconsComponent },
        { path: 'notifications', component: NotificationsComponent },
        { path: 'typography', component: TypographyComponent },
        { path: 'maps', component: MapsComponent }
      ]
    },
    { path: 'login', component: LoginComponent }
];
