package api.content;


import api.beacon.*;
import com.google.inject.*;
import java.util.*;

public class ContentService {

  @Inject
  private BeaconRepository beaconRepository;

  @Inject
  private ContentRepository contentRepository;

  public Content generateContent(String beaconId){
    List<Beacon> beacons = beaconRepository.find(beaconId);
    if (beacons.isEmpty()){
      return null;
    }
    Beacon beacon = beacons.get(0);
    if (beacon.hasContent()){
      return beacon.getContent();
    }
    return null;
  }

  public String generateContent(Long contentId){
    Content content = contentRepository.find(contentId);
    return content.getContent();
  }

}
