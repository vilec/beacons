package api.content;


import api.user.*;
import com.google.inject.*;
import java.util.*;
import org.mockito.*;
import play.db.jpa.*;

public class ContentRepository {

  @Inject
  private JPAApi jpaApi;

  @Inject
  private UserService userService;

  public List<Content> findAll() {
    return jpaApi.em().createQuery("SELECT b FROM Content b WHERE b.user.id = :userId")
      .setParameter("userId", userService.getLoggedUser().getId())
      .getResultList();
  }

  public Content find(Long id) {
    return jpaApi.em().createQuery("SELECT b FROM Content b WHERE id = :id", Content.class)
      .setParameter("id", id)
      .getSingleResult();
  }

  public void update(Content content) {
    jpaApi.em().merge(content);
  }

  public Content create(Content content) {
    jpaApi.em().persist(content);
    return content;
  }
}
