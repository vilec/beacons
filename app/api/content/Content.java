package api.content;


import api.user.*;
import com.fasterxml.jackson.annotation.*;
import javax.persistence.*;

@Entity
public class Content {

  public static final int CONTENT_LENGTH = 100;
  @Id
  @GeneratedValue(strategy=GenerationType.IDENTITY)
  public long id;
  public String name;
  public String content;

  @Enumerated(EnumType.STRING)
  public ContentType type;
  public String url;
  public String title;

  @OneToOne
  @JoinColumn(name="user_id")
  @JsonIgnore
  public User user;

  public boolean isOwnedBy(User user){
    return this.user.getId() == user.getId();
  }

  public String generateHTMLContent() {
    return content;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getContent() {
    return content != null && content.length() > CONTENT_LENGTH ? content.substring(0, CONTENT_LENGTH) + "..." : content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public ContentType getType() {
    return type;
  }

  public void setType(ContentType type) {
    this.type = type;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }
}
