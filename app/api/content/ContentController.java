package api.content;

import static api.content.ContentType.HTML;
import static api.content.ContentType.REDIRECT;
import api.request.*;
import api.security.SslEnforced;
import api.user.*;
import com.google.inject.*;
import java.util.*;
import play.db.jpa.*;
import play.libs.*;
import static play.libs.Json.toJson;
import play.mvc.*;
import views.html.*;

@SslEnforced
public class ContentController extends Controller {

  @Inject
  private ContentRepository contentRepository;

  @Inject
  private UserService user;

  @Inject
  private ContentService contentService;

  @Inject
  private RequestService requestService;

  @Transactional
  public Result test() {
    requestService.insertRequest(request());
    Content content = contentService.generateContent("ABCDE");
    if (content == null) {
      return notFound();
    }
    if (content.getType() == HTML) {
      return ok(test.render(content.generateHTMLContent()));
    } else if (content.getType() == REDIRECT) {
      return movedPermanently(content.getUrl());
    }
    return notFound();
  }

  @Transactional
  public Result show(String beaconId) {
    requestService.insertRequest(request());
    Content content = contentService.generateContent(beaconId);
    if (content == null) {
      return notFound();
    }
    if (content.getType() == HTML) {
      return ok(test.render(content.generateHTMLContent()));
    } else if (content.getType() == REDIRECT) {
      return movedPermanently(content.getUrl());
    }
    return notFound();
  }

  @Transactional
  public Result showContent(Long contentId) {
    String content = contentService.generateContent(contentId);
    return ok(content).as("text/html");
  }

  @Transactional
  public Result all() {
    List<Content> contents = contentRepository.findAll();
    return ok(toJson(contents));
  }

  @Transactional
  public Result get(Long id) {
    return ok(toJson(contentRepository.find(id)));
  }

  @Transactional
  public Result update() {
    Content content = Json.fromJson(request().body().asJson(), Content.class);
    Content oldContent = contentRepository.find(content.getId());
    if (!oldContent.isOwnedBy(user.getLoggedUser())) {
      return unauthorized();
    }
    content.setUser(user.getLoggedUser());
    contentRepository.update(content);
    return ok();
  }

  @Transactional
  public Result create() {
    Content content = Json.fromJson(request().body().asJson(), Content.class);
    content.setUser(user.getLoggedUser());
    return ok(toJson(contentRepository.create(content)));
  }

}
