package api.mail;

import api.security.SslEnforced;
import com.google.inject.Inject;
import play.data.Form;
import play.data.FormFactory;
import play.libs.mailer.Email;
import play.libs.mailer.MailerClient;
import play.mvc.Controller;
import play.mvc.Result;

@SslEnforced
public class MailController extends Controller {

  @Inject
  MailerClient mailerClient;

  @Inject
  FormFactory formFactory;

  public Result mail() {
    Form<EmailRequest> emailForm = formFactory.form(EmailRequest.class);
    EmailRequest request = emailForm.bindFromRequest().get();

    Email email = new Email()
      .setSubject("ohere.io - New Message")
      .setFrom(request.getName() + " <" + request.getEmail() + ">")
      .addTo("<hi@ohere.io>")
      .setBodyText(request.getMessage());
    mailerClient.send(email);
    return ok("Your message was successfully sent. Thank you!");
  }

  public Result mailmostostal() {
    Form<EmailRequest> emailForm = formFactory.form(EmailRequest.class);
    EmailRequest request = emailForm.bindFromRequest().get();

    Email email = new Email()
      .setSubject("Email from Mostostal stand")
      .setFrom(request.getName() + " <" + request.getEmail() + ">")
      .addTo("<katarzyna.wawrzenczyk@mostostal.com.pl>")
      .setBodyText(request.getMessage());
    mailerClient.send(email);
    return ok("Your message was successfully sent. Thank you!");
  }

}
