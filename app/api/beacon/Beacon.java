package api.beacon;

import api.content.*;
import api.user.*;
import com.fasterxml.jackson.annotation.*;
import javax.persistence.*;

@Entity
public class Beacon {

  @Id
  @GeneratedValue(strategy=GenerationType.IDENTITY)
  public long id;
  public String code;
  public String name;
  public String location;

  @OneToOne
  @JoinColumn(name="content_id")
  public Content content;

  @OneToOne
  @JoinColumn(name="user_id")
  @JsonIgnore
  public User user;

  public boolean isOwnedBy(User user){
    return this.user.getId() == user.getId();
  }

  public boolean hasContent(){
    return content != null;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getLocation() {
    return location;
  }

  public void setLocation(String location) {
    this.location = location;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public Content getContent() {
    return content;
  }

  public void setContent(Content content) {
    this.content = content;
  }
}
