package api.beacon;


import api.user.*;
import com.google.inject.*;
import java.util.*;
import play.db.jpa.*;
import play.mvc.*;

public class BeaconRepository {

  @Inject
  private JPAApi jpaApi;

  @Inject
  private UserService userService;

  public List<Beacon> findAll() {
    return jpaApi.em().createQuery("select b from Beacon b where b.user.id = :userId")
      .setParameter("userId", userService.getLoggedUser().getId())
      .getResultList();
  }

  public Beacon find(Long id) {
    return jpaApi.em().createQuery("select b from Beacon b where id = :id", Beacon.class)
      .setParameter("id", id)
      .getSingleResult();
  }

  public List<Beacon> find(String code) {
    return jpaApi.em().createQuery("select b from Beacon b where code = :code", Beacon.class)
      .setParameter("code", code)
      .getResultList();
  }

  public void update(Beacon beacon) {
    jpaApi.em().merge(beacon);
  }

  public Beacon create(Beacon beacon) {
    jpaApi.em().persist(beacon);
    return beacon;
  }
}
