package api.beacon;

import api.user.*;
import com.google.inject.*;
import org.apache.commons.lang3.*;

public class BeaconService {

  @Inject
  private BeaconRepository repository;

  @Inject
  private UserService user;

  public String generateBeaconId(){
    String generatedId = generateId();
    while (!repository.find(generatedId).isEmpty()){
      generatedId = generateId();
    }
    return generatedId;
  }

  private String generateId() {
    return RandomStringUtils.randomAlphabetic(5).toUpperCase();
  }

  public Beacon createBeacon(Beacon beacon) {
    beacon.setUser(user.getLoggedUser());
    beacon.setCode(generateBeaconId());
    repository.create(beacon);
    return beacon;

  }
}
