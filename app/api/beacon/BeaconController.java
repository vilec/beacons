package api.beacon;

import api.security.SslEnforced;
import api.user.*;
import com.google.inject.*;
import java.util.*;
import play.db.jpa.*;
import play.libs.*;
import static play.libs.Json.toJson;
import play.mvc.*;

@SslEnforced
public class BeaconController extends Controller {

  @Inject
  protected UserService user;

  @Inject
  private BeaconRepository beaconRepository;

  @Inject
  private BeaconService beaconService;

  @Transactional
  public Result beacons() {
    List<Beacon> persons = beaconRepository.findAll();
    return ok(toJson(persons));
  }

  @Transactional
  public Result beacon(Long id) {
    Beacon beacon = beaconRepository.find(id);
    if (!beacon.isOwnedBy(user.getLoggedUser())){
      return unauthorized();
    }
    return ok(toJson(beacon));
  }

  @Transactional
  public Result updateBeacon() {
    Beacon beacon = Json.fromJson(request().body().asJson(), Beacon.class);
    Beacon oldBeacon = beaconRepository.find(beacon.getId());
    if (!oldBeacon.isOwnedBy(user.getLoggedUser())){
      return unauthorized();
    }
    beacon.setUser(oldBeacon.getUser());
    beaconRepository.update(beacon);
    return ok();
  }

  @Transactional
  public Result createBeacon() {
    Beacon beacon = Json.fromJson(request().body().asJson(), Beacon.class);
    return ok(toJson(beaconService.createBeacon(beacon)));
  }

}
