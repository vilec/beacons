package api.request;

import api.beacon.*;
import com.google.inject.*;
import com.sun.org.apache.regexp.internal.RE;
import eu.bitwalker.useragentutils.*;
import java.util.*;
import java.util.stream.*;

import jdk.nashorn.internal.ir.RuntimeNode;
import play.mvc.*;

public class RequestService {

  @Inject
  private RequestRepository repository;

  @Inject
  private BeaconRepository beaconRepository;

  public void insertRequest(Http.Request incoming) {
    UserAgent userAgent = UserAgent.parseUserAgentString(incoming.getHeader("User-Agent"));
    Request request = new Request();
    applyBeaconAndContent(incoming, request);
    request.setAgent(incoming.getHeader("User-Agent"));
    request.setReferer(incoming.getHeader("Referer"));
    Browser browser = userAgent.getBrowser();
    request.setBrowser(browser != null ? browser.getName() : "google?");
    request.setOs(userAgent.getOperatingSystem().getName());
    request.setDeviceType(browser != null ? browser.getBrowserType().getName() : "google?");
    request.setIp(incoming.remoteAddress());
    request.setPath(incoming.path());
    request.setHeaders(convertToString(incoming.headers()));
    request.setCreated(new Date());
    request.setType(determineType(request));
    request.setOsType(determineOSType(request));
    repository.store(request);
  }

  private RequestType determineType(Request request) {
    if (request.getDeviceType().equals("Browser (mobile)")) {
      return RequestType.CLICK;
    } else if (request.getDeviceType().equals("Robot")) {
      return RequestType.ROBOT;
    } else if (request.getAgent().contains("PhysicalWeb")) {
      return RequestType.VIEW;
    } else {
      return RequestType.OTHER;
    }
  }

  private OSType determineOSType(Request request) {
    if (request.getOs().contains("Android")) {
      return OSType.ANDROID;
    } else if (request.getOs().contains("iPhone")) {
      return OSType.IOS;
    } else {
      return OSType.OTHER;
    }
  }

  private void applyBeaconAndContent(Http.Request incoming, Request request) {
    String beaconId = incoming.path().replace("/", "");
    List<Beacon> beacons = beaconRepository.find(beaconId);
    if (beacons.size() == 1) {
      Beacon beacon = beacons.get(0);
      request.setBeacon(beacon);
      request.setContent(beacon.getContent());
      request.setUser(beacon.getUser());
    }
  }

  private String convertToString(Map<String, String[]> headers) {
    return headers.entrySet().stream()
      .map(e -> e.getKey() + ": " + arrayToString(e.getValue()))
      .collect(Collectors.joining(" | "));
  }

  private String arrayToString(String[] array) {
    return Arrays.asList(array).stream().collect(Collectors.joining(","));
  }
}
