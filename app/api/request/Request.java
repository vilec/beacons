package api.request;

import api.beacon.*;
import api.content.*;
import api.user.*;
import com.fasterxml.jackson.annotation.*;
import java.util.*;
import javax.persistence.*;

@Entity
public class Request {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  public long id;

  public String ip;
  public String agent;
  public String referer;
  public String path;
  public String headers;
  public Date created;
  public String os;
  public String browser;

  @Enumerated(EnumType.STRING)
  public RequestType type;

  @Column(name = "os_type")
  @Enumerated(EnumType.STRING)
  public OSType osType;

  @Column(name = "device_type")
  public String deviceType;


  @OneToOne
  @JoinColumn(name = "beacon_id")
  @JsonIgnore
  public Beacon beacon;

  @OneToOne
  @JoinColumn(name = "content_id")
  @JsonIgnore
  public Content content;

  @OneToOne
  @JoinColumn(name = "user_id")
  @JsonIgnore
  public User user;

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getIp() {
    return ip;
  }

  public void setIp(String ip) {
    this.ip = ip;
  }

  public String getAgent() {
    return agent;
  }

  public void setAgent(String agent) {
    this.agent = agent;
  }

  public String getPath() {
    return path;
  }

  public void setPath(String body) {
    this.path = body;
  }

  public String getHeaders() {
    return headers;
  }

  public void setHeaders(String headers) {
    this.headers = headers;
  }

  public Beacon getBeacon() {
    return beacon;
  }

  public void setBeacon(Beacon beacon) {
    this.beacon = beacon;
  }

  public Content getContent() {
    return content;
  }

  public void setContent(Content content) {
    this.content = content;
  }

  public Date getCreated() {
    return created;
  }

  public void setCreated(Date created) {
    this.created = created;
  }

  public String getReferer() {
    return referer;
  }

  public void setReferer(String referer) {
    this.referer = referer;
  }

  public String getOs() {
    return os;
  }

  public void setOs(String os) {
    this.os = os;
  }

  public String getBrowser() {
    return browser;
  }

  public void setBrowser(String browser) {
    this.browser = browser;
  }

  public String getDeviceType() {
    return deviceType;
  }

  public void setDeviceType(String deviceType) {
    this.deviceType = deviceType;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public RequestType getType() {
    return type;
  }

  public void setType(RequestType type) {
    this.type = type;
  }

  public OSType getOsType() {
    return osType;
  }

  public void setOsType(OSType osType) {
    this.osType = osType;
  }
}
