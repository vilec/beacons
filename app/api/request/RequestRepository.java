package api.request;

import com.google.inject.*;
import play.db.jpa.*;

public class RequestRepository {

  @Inject
  private JPAApi jpaApi;

  public void store(Request request) {
    jpaApi.em().persist(request);
  }


}
