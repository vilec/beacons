package api.request;

public enum OSType {
  ANDROID("Android"), IOS("iOS"), OTHER("Other");

  private final String name;

  OSType(String name) {
    this.name = name;
  }

  public String getName(){
    return name;
  }
}
