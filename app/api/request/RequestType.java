package api.request;

public enum RequestType {
  VIEW, CLICK, ROBOT, OTHER
}
