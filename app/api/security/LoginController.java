package api.security;

import api.user.*;
import com.google.inject.*;
import java.util.*;
import play.data.*;
import play.db.jpa.*;
import play.libs.*;
import static play.libs.Json.toJson;
import play.mvc.*;

@SslEnforced
public class LoginController extends Controller {

  @Inject
  private FormFactory formFactory;

  @Inject
  private UserRepository userRepository;

  @Transactional
  public Result login() {
    Form<Login> loginForm = formFactory.form(Login.class);
    Login login = loginForm.bindFromRequest().get();

    List<User> users = userRepository.findUsers(login.getUsername());
    if (users.size() != 1) {
      return unauthorized();
    }

    User user = users.get(0);
    if (user.validatePassword(login.getPassword())){
      return ok(toJson(Json.newObject()
        .put("token", user.generateToken())));
    }
    return unauthorized();
  }
}
