package api;

import api.security.SslEnforced;
import play.data.*;
import play.db.jpa.*;
import play.mvc.*;
import views.html.*;

@SslEnforced
public class Application extends Controller {

  public Application(){}

  public Application(FormFactory formFactory, JPAApi jpaApi) {

  }

  public Result index(String any) {
    return ok(index.render());
  }

  public Result landing() {
    return ok(landing.render());
  }

}
