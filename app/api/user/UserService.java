package api.user;

import com.google.inject.*;
import java.util.*;
import org.pac4j.core.profile.*;
import org.pac4j.play.*;
import org.pac4j.play.store.*;
import play.mvc.*;

public class UserService {

  @Inject
  private PlaySessionStore playSessionStore;

  public User getLoggedUser() {
    PlayWebContext webContext = new PlayWebContext(Http.Context.current(), playSessionStore);
    ProfileManager<CommonProfile> profileManager = new ProfileManager(webContext);
    Optional<CommonProfile> profile = profileManager.get(false);
    User user = new User();
    user.setId((Long) profile.get().getAttribute("id"));
    return user;
  }
}
