package api.user;


import api.security.SslEnforced;
import com.google.inject.*;
import play.data.*;
import play.db.jpa.*;
import play.mvc.*;

@SslEnforced
public class UserController extends Controller {

  @Inject
  private JPAApi jpaApi;

  @Inject
  private FormFactory formFactory;

  @Transactional
  public Result create() {
    Form<User> userForm = formFactory.form(User.class);
    User user = userForm.bindFromRequest().get();
    jpaApi.em().persist(user);
    return ok();
  }
}
