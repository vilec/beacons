package api.user;


import api.security.*;
import javax.persistence.*;
import org.mindrot.jbcrypt.*;
import org.pac4j.core.profile.*;
import org.pac4j.jwt.config.signature.*;
import org.pac4j.jwt.profile.*;

@Entity
public class User {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private long id;
  private String username;
  private String password;
  private String email;

  public boolean validatePassword(String candidate) {
    return BCrypt.checkpw(candidate, password);
  }

  public String generateToken() {
    CommonProfile profile = new CommonProfile();
    profile.addAttribute("username", username);
    profile.addAttribute("email", email);
    profile.addAttribute("id", id);
    return new JwtGenerator(new SecretSignatureConfiguration(SecurityModule.SECRET)).generate(profile);
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = BCrypt.hashpw(password, BCrypt.gensalt());
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }
}
