package api.report.dto;

import java.util.List;

public class ChartReport {
  private List<ChartReportSeries> series;
  private List<String> labels;

  public ChartReport(List<ChartReportSeries> series, List<String> labels) {
    this.series = series;
    this.labels = labels;
  }

  public List<ChartReportSeries> getSeries() {
    return series;
  }

  public void setSeries(List<ChartReportSeries> series) {
    this.series = series;
  }

  public List<String> getLabels() {
    return labels;
  }

  public void setLabels(List<String> labels) {
    this.labels = labels;
  }
}
