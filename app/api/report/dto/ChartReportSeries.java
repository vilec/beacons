package api.report.dto;

import java.util.*;


public class ChartReportSeries {

  private List<Long> data;
  private String label;

  public ChartReportSeries(String label) {
    this.label = label;
    this.data = new ArrayList<>();
  }

  public List<Long> getData() {
    return data;
  }

  public void setData(List<Long> data) {
    this.data = data;
  }

  public String getLabel() {
    return label;
  }

  public void setLabel(String label) {
    this.label = label;
  }
}
