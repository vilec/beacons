package api.report.dto;


import java.util.*;

public class PieReport {


  public PieReport() {
    this.data = new ArrayList<>();
    this.labels = new ArrayList<>();
  }

  public List<Integer> data;
  public List<String> labels;

}
