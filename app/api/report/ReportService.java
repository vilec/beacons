package api.report;

import api.report.dto.ChartReport;
import api.report.dto.ChartReportSeries;
import api.report.dto.PieReport;
import api.report.dto.StatsReport;
import api.request.*;
import static api.request.RequestType.CLICK;
import static api.request.RequestType.VIEW;

import com.google.inject.*;

import java.math.BigDecimal;
import java.util.*;

import org.joda.time.*;

public class ReportService {

  public static final int LAST_DAYS = 7;
  @Inject
  private ReportRepository repository;

  public PieReport generateOsBreakdown() {
    List<Request> requests = repository.allClicks();
    PieReport pieReport = new PieReport();
    Arrays.asList(OSType.values()).forEach(os -> {
      pieReport.data.add(Long.valueOf(requests.stream().filter(r -> r.getOsType() == os).count()).intValue());
      pieReport.labels.add(os.getName());
    });
    return pieReport;
  }

  public ChartReport generateLastDaysReport() {
    List<String> labels = new ArrayList<>();
    List<Request> requests = repository.lastDaysReport(LAST_DAYS);
    List<ChartReportSeries> series = new ArrayList<>();
    ChartReportSeries clicks = new ChartReportSeries("# of Clicks");
    ChartReportSeries views = new ChartReportSeries("# of Views");
    LocalDate startDate = LocalDate.now().minusDays(LAST_DAYS - 1);
    LocalDate endDate = startDate.minusDays(1);
    for (int i = 0; i < LAST_DAYS; i++) {
      labels.add(startDate.toString("MM/dd"));
      populateClicksSeries(requests, clicks, startDate, endDate);
      populateViewsSeries(requests, views, startDate, endDate);
      startDate = startDate.plusDays(1);
      endDate = endDate.plusDays(1);
    }
    series.add(clicks);
    series.add(views);
    return new ChartReport(series, labels);
  }

  private void populateClicksSeries(List<Request> requests, ChartReportSeries series, LocalDate startDate, LocalDate endDate) {
    series.getData().add(requests.stream()
      .filter(r -> r.getType() == CLICK && isBetween(startDate, endDate, r.getCreated()))
      .count());

  }
  private void populateViewsSeries(List<Request> requests, ChartReportSeries series, LocalDate startDate, LocalDate endDate) {
    series.getData().add(requests.stream()
      .filter(r -> isBetween(startDate, endDate, r.getCreated()))
      .count());
  }

  private boolean isBetween(final LocalDate startDate, final LocalDate endDate, Date created) {
    LocalDate createDate = new LocalDate(created);
    return createDate.isEqual(startDate) || (createDate.isAfter(startDate) && createDate.isBefore(endDate));
  }


  public StatsReport getStatsReport() {
    List<Request> requests = repository.allClicksViews();
    StatsReport statsReport = new StatsReport();
    statsReport.clicks = requests.stream().filter(r -> r.getType() == CLICK).count();
    statsReport.views = requests.stream().filter(r -> r.getType() == VIEW).count();
    if (statsReport.views == 0){
      statsReport.engage = "0";
    } else {
      statsReport.engage = new BigDecimal(statsReport.clicks)
        .divide(new BigDecimal(statsReport.views), 4, BigDecimal.ROUND_DOWN)
        .multiply(new BigDecimal(100))
        .setScale(1, BigDecimal.ROUND_DOWN)
        .toString();
    }
    return statsReport;
  }

  public List<TopReport> getTopContentReport(){
    return  repository.topContens(5);
  }
  public List<TopReport> getTopBeaconReport(){
    return  repository.topBeacons(5);
  }
}
