package api.report;


public class TopReport {
  public String label;
  public long count;

  public TopReport(String label, long count) {
    this.label = label;
    this.count = count;
  }
}
