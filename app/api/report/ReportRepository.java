package api.report;

import api.request.*;
import static api.request.RequestType.CLICK;
import static api.request.RequestType.VIEW;

import api.user.UserService;
import com.google.inject.*;
import java.util.*;
import org.joda.time.*;
import play.db.jpa.*;

public class ReportRepository {

  @Inject
  private JPAApi jpaApi;

  @Inject
  private UserService userService;

  public List<Request> lastDaysReport(int lastDays) {
    return jpaApi.em().createQuery("SELECT r FROM Request r WHERE" +
      " r.type in :types and r.created >= :startDay and r.user.id = :userId")
      .setParameter("types", Arrays.asList(VIEW, CLICK))
      .setParameter("startDay", DateTime.now().minusDays(lastDays).withTimeAtStartOfDay().toDate())
      .setParameter("userId", userService.getLoggedUser().getId())
      .getResultList();
  }

  public List<Request> allClicks() {
    return jpaApi.em().createQuery("SELECT r FROM Request r WHERE r.type in :types AND r.user.id = :userId")
      .setParameter("types", Arrays.asList(CLICK))
      .setParameter("userId", userService.getLoggedUser().getId())
      .getResultList();
  }

  public List<Request> allClicksViews() {
    return jpaApi.em().createQuery("SELECT r FROM Request r WHERE r.type in :types AND r.user.id = :userId")
      .setParameter("types", Arrays.asList(CLICK, VIEW))
      .setParameter("userId", userService.getLoggedUser().getId())
      .getResultList();
  }

  public List<TopReport> topContens(int limit) {
    return jpaApi.em().createQuery("SELECT new api.report.TopReport(c.name, count(*)) " +
      "FROM Request r JOIN r.content c WHERE r.type in :types AND r.user.id = :userId GROUP BY c.name order by count(*) desc")
      .setParameter("types", Arrays.asList(CLICK))
      .setParameter("userId", userService.getLoggedUser().getId())
      .setMaxResults(limit)
      .getResultList();
  }

  public List<TopReport> topBeacons(int limit) {
    return jpaApi.em().createQuery("SELECT new api.report.TopReport(b.name, count(*)) " +
      "FROM Request r JOIN r.beacon b WHERE r.type in :types AND r.user.id = :userId GROUP BY b.name order by count(*) desc")
      .setParameter("types", Arrays.asList(VIEW))
      .setParameter("userId", userService.getLoggedUser().getId())
      .setMaxResults(limit)
      .getResultList();
  }

}
