package api.report;


import api.security.SslEnforced;
import com.google.inject.*;
import java.util.*;
import play.db.jpa.*;
import static play.libs.Json.toJson;
import play.mvc.*;

@SslEnforced
public class ReportController extends Controller {

  @Inject
  private ReportService service;

  @Transactional
  public Result getOsBreakdown() {
    return ok(toJson(service.generateOsBreakdown()));
  }

  @Transactional
  public Result getLastDaysReport() {
    return ok(toJson(service.generateLastDaysReport()));
  }

  @Transactional
  public Result getStatsReport() {
    return ok(toJson(service.getStatsReport()));
  }

  @Transactional
  public Result getTopContentReport() {
    return ok(toJson(service.getTopContentReport()));
  }

  @Transactional
  public Result getTopBeaconReport() {
    return ok(toJson(service.getTopBeaconReport()));
  }
}
