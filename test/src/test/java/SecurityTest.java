package src.test.java;

import com.fasterxml.jackson.databind.*;
import static junit.framework.TestCase.assertNotNull;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import org.junit.*;
import play.libs.*;
import play.mvc.*;
import static play.test.Helpers.*;
import play.test.*;

public class SecurityTest extends WithApplication {

  @Override
  protected play.Application provideApplication() {
    return fakeApplication(inMemoryDatabase());
  }

  @Test
  public void unauthroized() {
    Http.RequestBuilder request = new Http.RequestBuilder()
      .method(GET)
      .uri("/api/beacons");

    Result result = route(app, request);
    assertThat(result.status(), is(401));
  }

  @Test
  public void loginFailed() {
    Http.RequestBuilder login = new Http.RequestBuilder()
      .method(PUT)
      .bodyJson(Json.newObject()
        .put("username", "aaa")
        .put("password", "bbb")
      )
      .uri("/api/login");

    assertThat(route(app, login).status(), is(401));
  }

  @Test
  public void loginSuccessful() {
    Http.RequestBuilder createUser = new Http.RequestBuilder()
      .method(PUT)
      .bodyJson(Json.newObject()
        .put("username", "aaa")
        .put("password", "bbb")
        .put("email", "aaa@bbb.com")
      )
      .uri("/api/user");

    assertThat(route(app, createUser).status(), is(200));

    Http.RequestBuilder login = new Http.RequestBuilder()
      .method(PUT)
      .bodyJson(Json.newObject()
        .put("username", "test")
        .put("password", "test")
      )
      .uri("/api/login");

    JsonNode node = Json.parse(contentAsString(route(app, login)));
    assertNotNull(node.get("token"));

    Http.RequestBuilder authorizedRequest = new Http.RequestBuilder()
      .header("Authorization", "Bearer " + node.get("token").textValue())
      .method(GET)
      .uri("/api/beacons");

    assertThat(route(app, authorizedRequest).status(), is(200));
  }
}
