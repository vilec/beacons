package src.test.java;

import com.fasterxml.jackson.databind.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import org.junit.*;
import play.libs.*;
import play.mvc.*;
import static play.test.Helpers.*;

public class BeaconTest extends WithBeaconApplication {

  @Test
  public void createAndRetrieveBeacon() {
    Http.RequestBuilder beacon = new Http.RequestBuilder()
      .headers(AUTH_HEADER)
      .method(POST)
      .bodyJson(Json.newObject()
        .put("name", "Beacon A")
        .put("location", "Location A")
      )
      .uri("/api/beacon");

    assertThat(route(app, beacon).status(), is(200));

    Http.RequestBuilder getBeacon = new Http.RequestBuilder()
      .headers(AUTH_HEADER)
      .method(GET)
      .uri("/api/beacon/1");

    Result result = route(app, getBeacon);
    assertThat(result.status(), is(200));
    JsonNode json = Json.parse(contentAsString(result));
    assertThat(json.get("name").asText(), is("Beacon A"));
    assertThat(json.get("location").asText(), is("Location A"));
  }

  @Test
  public void createAndUpdateBeacon() {

    // Create beacon
    Http.RequestBuilder beacon = new Http.RequestBuilder()
      .headers(AUTH_HEADER)
      .method(POST)
      .bodyJson(Json.newObject()
        .put("name", "Beacon C")
        .put("location", "Location C")
      )
      .uri("/api/beacon");
    route(app, beacon);

    // Update Beacon
    Http.RequestBuilder updateBeacon = new Http.RequestBuilder()
      .headers(AUTH_HEADER)
      .method(PUT)
      .bodyJson(Json.newObject()
        .put("id", 1)
        .put("name", "Beacon D")
        .put("location", "Location D")
      )
      .uri("/api/beacon");
    route(app, updateBeacon);

    // Check if updated
    Http.RequestBuilder getBeacon = new Http.RequestBuilder()
      .headers(AUTH_HEADER)
      .method(GET)
      .uri("/api/beacon/1");

    JsonNode json = Json.parse(contentAsString(route(app, getBeacon)));
    assertThat(json.get("name").asText(), is("Beacon D"));
    assertThat(json.get("location").asText(), is("Location D"));
  }

  @Test
  public void getAllBeacons() {

    // Create beacon
    Http.RequestBuilder beacon = new Http.RequestBuilder()
      .headers(AUTH_HEADER)
      .method(POST)
      .bodyJson(Json.newObject()
        .put("name", "Beacon A")
        .put("location", "Location A")
      )
      .uri("/api/beacon");
    route(app, beacon);

    Http.RequestBuilder beacon2 = new Http.RequestBuilder()
      .headers(AUTH_HEADER)
      .method(POST)
      .bodyJson(Json.newObject()
        .put("name", "Beacon B")
        .put("location", "Location B")
      )
      .uri("/api/beacon");
    route(app, beacon2);

    // Get all beacons
    Http.RequestBuilder getBeacons = new Http.RequestBuilder()
      .headers(AUTH_HEADER)
      .method(GET)
      .uri("/api/beacons");

    JsonNode json = Json.parse(contentAsString(route(app, getBeacons)));
    assertThat(json.size(), is(2));
    assertThat(json.get(0).get("name").asText(), is("Beacon A"));
    assertThat(json.get(0).get("location").asText(), is("Location A"));
    assertThat(json.get(1).get("name").asText(), is("Beacon B"));
    assertThat(json.get(1).get("location").asText(), is("Location B"));
  }


}
