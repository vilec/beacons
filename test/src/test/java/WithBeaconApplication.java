package src.test.java;

import com.google.common.collect.*;
import java.util.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import play.*;
import play.libs.*;
import play.mvc.*;
import play.test.*;
import static play.test.Helpers.*;

public class WithBeaconApplication extends WithApplication {

  protected final Map<String, String[]> AUTH_HEADER = Maps.newHashMap(ImmutableMap.<String, String[]>builder()
  .put("Authorization", new String[] {"Bearer eyJhbGciOiJIUzI1NiJ9.eyIkaW50X3Blcm1zIjpbXSwic3ViIjoib3JnLnBhYzRqLmNvcmUucHJvZmlsZS5Db21tb25Qcm9maWxlI251bGwiLCIkaW50X3JvbGVzIjpbXSwiaWF0IjoxNDg5MzUxMDk3LCJlbWFpbCI6InRlc3RAdGVzdC5jb20iLCJ1c2VybmFtZSI6InRlc3QifQ.w4ZELVko2yf3UHJ_rsvrRWIY86J1j3cnC07XLaU7faI"})
  .build());

  @Override
  protected Application provideApplication() {
    return fakeApplication(inMemoryDatabase());
  }

  @Override
  public void startPlay() {
    super.startPlay();
    Http.RequestBuilder createUser = new Http.RequestBuilder()
      .method(PUT)
      .bodyJson(Json.newObject()
        .put("username", "test")
        .put("password", "test")
        .put("email", "test@test.com")
      )
      .uri("/api/user");
    assertThat(route(app, createUser).status(), is(200));
  }
}
