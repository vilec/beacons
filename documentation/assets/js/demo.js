type = ['','info','success','warning','danger'];


demo = {
	showNotification: function(from, align){
    	color = Math.floor((Math.random() * 4) + 1);

    	$.notify({
        	icon: "pe-7s-gift",
        	message: "Welcome to <b>ohere Platform</b> - a powerful beacons and content management system."

        },{
            type: 'info',
            timer: 4000,
            placement: {
                from: from,
                align: align
            }
        });
	}
}
