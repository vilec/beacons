$(document).ready(function () {

    $('#fullpage').fullpage({
       // sectionsColor: ['#ffffff', '#ffffff', '#ffffff'],
        anchors: ['home', 'solution', 'faq', 'contact'],
        menu: '#menu',
        autoScrolling: false,
        responsiveWidth: 768,
        navigation: false,
        navigationPosition: 'right',
        navigationTooltips: ['home', 'solution', 'faq', 'contact'],
        sectionSelector: '.stage',
        verticalCentered: true,
        showActiveTooltip: false,
        fitToSection: false,
        afterLoad: function(anchorLink, index){

        },
        onLeave: function(index, nextIndex, direction) {
        }
    });


});
