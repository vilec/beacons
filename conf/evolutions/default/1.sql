# Users schema

# --- !Ups

CREATE TABLE `user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=UTF8;

CREATE TABLE `content` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `type` varchar(255) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `user_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `conter_user_fk` (`user_id`),
  CONSTRAINT `conter_user_fk` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

CREATE TABLE `beacon` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(5) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `location` varchar(255) DEFAULT NULL,
  `content_id` int(11) unsigned DEFAULT NULL,
  `user_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `content_fk` (`content_id`),
  KEY `user_fk` (`user_id`),
  CONSTRAINT `content_fk` FOREIGN KEY (`content_id`) REFERENCES `content` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `user_fk` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

CREATE TABLE `request` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip` varchar(255) DEFAULT NULL,
  `beacon_id` int(11) unsigned DEFAULT NULL,
  `content_id` int(11) unsigned DEFAULT NULL,
  `user_id` int(11) unsigned DEFAULT NULL,
  `path` varchar(255) DEFAULT NULL,
  `created` datetime NOT NULL,
  `device_type` varchar(255) DEFAULT NULL,
  `os` varchar(255) DEFAULT NULL,
  `browser` varchar(255) DEFAULT NULL,
  `agent` varchar(255) DEFAULT NULL,
  `referer` varchar(255) DEFAULT NULL,
  `headers` text,
  PRIMARY KEY (`id`),
  KEY `request_beacon_fk` (`beacon_id`),
  KEY `request_content_fk` (`content_id`),
  KEY `request_user_fk` (`user_id`),
  CONSTRAINT `request_beacon_fk` FOREIGN KEY (`beacon_id`) REFERENCES `beacon` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `request_content_fk` FOREIGN KEY (`content_id`) REFERENCES `content` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `request_user_fk` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

INSERT INTO `user` (`id`, `username`, `password`, `email`)
VALUES
	(1, 'vilec', '$2a$10$V6Va5oTwK9ugIajLMWcPJ.1wIZpz/CcW5GsWBqbqy7pYrMKt46GRm', 'vilec@test.com');

INSERT INTO `content` (`id`, `name`, `type`, `content`, `url`, `title`, `user_id`)
VALUES
	(2, 'Simple Text', 'HTML', 'Simple test', NULL, 'Test Title | Nice', 1),
	(3, 'Invitation', 'HTML', '<h1><strong>Welcome</strong> to my page! </h1>\n<a href=\"http://digitalwolves.io\">Go Check My Website</a>', NULL, 'Welcome Matii!', 1),
	(4, 'Redirect to Digital Wolves', 'REDIRECT', NULL, 'http://digitalwolves.io', NULL, 1),
	(6, 'Redirect to webinsideltd.com', 'REDIRECT', NULL, 'http://webinsideltd.com', NULL, 1);

INSERT INTO `beacon` (`id`, `code`, `name`, `location`, `content_id`, `user_id`)
VALUES
	(4, 'NGYYM', 'Beacon Kontakt.io #1', 'Main Room', 4, 1),
	(5, 'MNZLN', 'Beacon Kontakt.io #2', 'Living Room', 4, 1),
	(6, 'ICNBS', 'My Personal Beacon', 'My Bedroom', 6, 1),
	(7, 'GOWIF', 'Beacon Estimote #1', 'White Star Power Gym', 3, 1),
	(8, 'BLVQS', 'Beacon Estimote #2', 'White Star Power Gym', NULL, 1),
	(9, 'TLBOB', 'Mostostal Beacon', 'Building Trade', 3, 1),
	(10, 'ANVIE', 'Nowy beacon', 'Moj pokoj', NULL, 1);


# --- !Downs

DROP TABLE request;
DROP TABLE beacon;
DROP TABLE content;
DROP TABLE user;
