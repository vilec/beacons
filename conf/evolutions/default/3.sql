# --- !Ups
ALTER TABLE request add  `os_type` varchar(255) NOT NULL DEFAULT 'OTHER';
update request set os_type = 'ANDROID' where os like '%Android%';
update request set os_type = 'IOS' where os like '%iPhone%';

# --- !Downs
alter table request drop column os_type;
