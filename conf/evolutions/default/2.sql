# --- !Ups
ALTER TABLE request add  `type` varchar(255) NOT NULL DEFAULT 'OTHER';
update request set type = 'ROBOT' where device_type = 'Robot';
update request set type = 'CLICK' where device_type = 'Browser (mobile)';
update request set type = 'VIEW' where agent like '%PhysicalWeb%';

# --- !Downs
alter table request drop column type;
